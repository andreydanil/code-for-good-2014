#String detection module - Code for Good 2014

from Tkinter import *

def timerFired(canvas):
    redrawAll(canvas)
    delay = 100
    canvas.after(delay, lambda: timerFired(canvas))

def redrawAll(canvas):
    canvas.delete(ALL)
    drawBoard(canvas)

def drawBoard(canvas):
    output = ""
    for tag in canvas.data.detected:
        if (output == ''):
            output = output + tag
        else:
            output = output + ", " + tag
    canvas.create_text(10,105,text= "The detected tags are: " + output,anchor=W)

def detectTags(canvas):
    canvas.data.description = canvas.data.descriptEntry.get()
    canvas.data.tags = canvas.data.tagsEntry.get().split()
    (description,tags) = (canvas.data.description,canvas.data.tags)
    #Separates the tags according to spaces to split into a list
    canvas.data.detected = []
    for tag in tags:
        if ((" " + tag.lower() + " " in description.lower()) or\
            (tag[0:len(tag)].lower() + " " == description[0:len(tag)+1].lower()) or\
            (" " + tag[0:len(tag)].lower() == description[len(description)-len(tag)-1:len(description)].lower())):
            canvas.data.detected.append(tag)
    
    
def init(canvas):
    #Placing all the elements on the page
    canvas.data.center = [150,140]
    canvas.data.descriptLabel = Message(canvas, text="Description",width = 90)
    canvas.data.tagsLabel = Message(canvas, text="Tags (Separate by spaces)",width = 90)
    canvas.data.descriptEntry = Entry(canvas, width = 80)
    canvas.data.tagsEntry = Entry(canvas, width = 80)
    canvas.data.descriptLabel.place(x=5,y=10)
    canvas.data.descriptEntry.place(x=100,y=10)
    canvas.data.tagsLabel.place(x=5,y=30)
    canvas.data.tagsEntry.place(x=100,y=30)
    canvas.data.button = Button(canvas,text="Display",width=50,command = lambda: detectTags(canvas))
    canvas.data.button.place(x=20,y=70)
    canvas.data.detected = []
    redrawAll(canvas)

def Run():
    root = Tk()
    root.wm_title("Tag detector")
    canvasWidth = 600
    canvasHeight = 150
    canvas = Canvas(root, width=canvasWidth, height=canvasHeight)
    canvas.pack()
    class Struct: pass
    canvas.data = Struct()
    root.canvas = canvas.canvas = canvas
    init(canvas)
    timerFired(canvas)
    root.mainloop()

Run()