National wildlife federation
============================

Connecting wildfile gardeners
  mission: to inspire americans to protect wildlife for our children's future
  challenge: providing food food wildfile. Connect expert gardeners with local new gardeners.
    * how can they quickly identify local plant species tat will thrive
    * how to share expert advice
    * help the wildlife

American red cross
============================

Specializing in disaster management
  mission: helping with need
  who we serve:
    * people
    * money
    * stuff

  questions:
    * how to deliver stuff into a country after a disaster? store after disaster?
    * deploy 8 mill items in US. working 189 countries.
    * after warehouse, move to people.

  challenge: help deliver items. location based storage. provide relief items sooner. there are a lot of custom needs.
  example: bringing water after tsunami. stopping point, adapter between water pipes.

  goal: develop a workflow technology to enable the 3D printing process.
  steps: identify, confirm, crowd source, order.
