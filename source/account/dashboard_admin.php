<?php
/*

UserFrosting Version: 0.2.1 (beta)
By Alex Weissman
Copyright (c) 2014

Based on the UserCake user management system, v2.0.2.
Copyright (c) 2009-2012

UserFrosting, like UserCake, is 100% free and open-source.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

// UserCake authentication
require_once("../models/config.php");


if (!securePage(__FILE__)){
  // TODO: account section has its own 404 page
  header("Location: index.php");
  exit();
}


setReferralPage(getAbsoluteDocumentPath(__FILE__));

// Admin page
?>
<!DOCTYPE html>
<html lang="en">
  <?php
  	echo renderAccountPageHeader(array("#SITE_ROOT#" => SITE_ROOT, "#SITE_TITLE#" => SITE_TITLE, "#PAGE_TITLE#" => "Admin Dashboard"));
  ?>

  <body>    
    <div id="wrapper">

      <!-- Sidebar -->
        <?php
            echo renderMenu("dashboard-admin");
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div id='display-alerts' class="col-lg-12">

          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-12">
            <h1>Dashboard <small>Statistics Overview</small></h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
            </ol>
            
          </div>
        </div><!-- /.row -->

        <!--
        <div class="row" >
           <div class="col-lg-12">
       <form class="form-horizontal" role="form" name="updateAccount" action="update_user.php" method="post" padding-right: 0>
      <div class="form-group" align='left'>
      <label class="col-sm-4 control-label">New Item Name</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text1' value=' '>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Design Specifications</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text3'>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Printing Location</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text'>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Item Description</label>
      <div class="col-sm-4">
        <input type="text" rows=3 class="form-control" placeholder="" name='text5'>
      </div>
      </div>
      
      <div class="form-group">
      <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" class="btn btn-success submit" value='Update'>Update</button>
      </div>
      </div>
      <input type="hidden" name="csrf_token" value="<?php echo $loggedInUser->csrf_token; ?>" />
      <input type="hidden" name="user_id" value="0" />
      </form>
            </div>
          </div>

           <div class="row" >
           <div class="col-lg-12">
       <form class="form-horizontal" role="form" name="updateAccount" action="update_user.php" method="post" padding-right: 0>
      <div class="form-group" align='left'>
      <label class="col-sm-4 control-label">New Item Name</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text1' value=' '>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Design Specifications</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text3'>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Printing Location</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text'>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Item Description</label>
      <div class="col-sm-4" rows='3'>
       <textarea class="form-control" rows="4"></textarea>
      </div>
      </div>

           <div class="row" >
           <div class="col-lg-12">
       <form class="form-horizontal" role="form" name="updateAccount" action="update_user.php" method="post" padding-right: 0>
      <div class="form-group" align='left'>
      <label class="col-sm-4 control-label">New Item Name</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text1' value=' '>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Design Specifications</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text3'>
      </div>
      </div>
      <div class="form-group">
      <label class="col-sm-4 control-label">Printing Location</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" placeholder="" name='text'>
      </div>
      </div>
       <div class="form-group">
      <label class="col-sm-4 control-label">Printing Location</label>
      <div class="col-sm-4">
        <input type="file" data-filename-placement="inside">
      </div>
      </div>
<script type="text/javascript">
$('input[id=lefile]').change(function() {
$('#photoCover').val($(this).val());
});
</script>
      
      <div class="form-group">
      <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" class="btn btn-success submit" value='Update'>Update</button>
      </div>
      </div>
      <input type="hidden" name="csrf_token" value="<?php echo $loggedInUser->csrf_token; ?>" />
      <input type="hidden" name="user_id" value="0" />
      </form>
            </div>
          </div>-->

        <div class="row">
          <div class="col-lg-3">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading">3</p>
                    <p class="announcement-text">Requests</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="requests.php">View requests</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="panel panel-warning">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-check fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading">3</p>
                    <p class="announcement-text">Pending requests</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <a href="design">Pending Tasks</a>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div><!-- /.row -->

        <!--<div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Traffic Statistics: October 1, 2013 - October 31, 2013</h3>
              </div>
              <div class="panel-body">
                <div id="morris-chart-area"></div>
              </div>
            </div>
          </div>
        </div> 

        <div class="row">
          <div class="col-lg-4">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Traffic Sources: October 1, 2013 - October 31, 2013</h3>
              </div>
              <div class="panel-body">
                <div id="morris-chart-donut"></div>
                <div class="text-right">
                  <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
          -->
          <div class="col-lg-4">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o"></i> Recent Activity</h3>
              </div>
              <div class="panel-body">
                <div class="list-group">
                  <a href="#" class="list-group-item">
                    <span class="badge">just now</span>
                    <i class="fa fa-calendar"></i> Calendar updated
                  </a>
                  <a href="#" class="list-group-item">
                    <span class="badge">4 minutes ago</span>
                    <i class="fa fa-comment"></i> Commented on a post
                  </a>
                  <a href="#" class="list-group-item">
                    <span class="badge">23 minutes ago</span>
                    <i class="fa fa-truck"></i> Order 392 shipped
                  </a>
                  <a href="#" class="list-group-item">
                    <span class="badge">46 minutes ago</span>
                    <i class="fa fa-money"></i> Invoice 653 has been paid
                  </a>
                  <a href="#" class="list-group-item">
                    <span class="badge">1 hour ago</span>
                    <i class="fa fa-user"></i> A new user has been added
                  </a>
                  <a href="#" class="list-group-item">
                    <span class="badge">yesterday</span>
                    <i class="fa fa-globe"></i> Saved the world
                  </a>
                  <a href="#" class="list-group-item">
                    <span class="badge">two days ago</span>
                    <i class="fa fa-check"></i> Completed task: "fix error on sales page"
                  </a>
                </div>
                <div class="text-right">
                  <a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-history"></i> Requests</h3>
              </div>
              <div class="panel-body">
                  <div id="transactions" class="table-responsive">
                  <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                      <tr>
                        <th>Order # <i class="fa fa-sort"></i></th>
                        <th>Order Date <i class="fa fa-sort"></i></th>
                        <th>Order Time <i class="fa fa-sort"></i></th>
                        <th>Amount (USD) <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>hammer, mechanics</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>hammer, mechanics</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>tools</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      
                    </tbody>
                  </table>
                </div>
                <div class="text-right">
                  <a href="#">View All Requests <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.row -->


      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->
    
    <script src="../js/raphael/2.1.0/raphael-min.js"></script>
    <script src="../js/morris/morris-0.4.3.js"></script>
    <script src="../js/morris/chart-data-morris.js"></script>
    <script>
        $(document).ready(function() {          
          alertWidget('display-alerts');
          
          // Initialize the transactions tablesorter
          $('#transactions .table').tablesorter({
              debug: false
          });
          
        });      
    </script>
  </body>
</html>
