<?php
/*

UserFrosting Version: 0.2.1 (beta)
By Alex Weissman
Copyright (c) 2014

Based on the UserCake user management system, v2.0.2.
Copyright (c) 2009-2012

UserFrosting, like UserCake, is 100% free and open-source.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

require_once("../models/config.php");

if (!securePage(__FILE__)){
  // Forward to index page
  addAlert("danger", "Whoops, looks like you don't have permission to view that page.");
  header("Location: index.php");
  exit();
}

setReferralPage(getAbsoluteDocumentPath(__FILE__));

?>

<!DOCTYPE html>
<html lang="en">
  <?php
  	echo renderAccountPageHeader(array("#SITE_ROOT#" => SITE_ROOT, "#SITE_TITLE#" => SITE_TITLE, "#PAGE_TITLE#" => "Requests"));
  ?>

  <body>
    <div id="wrapper">

      <!-- Sidebar -->
        <?php
          echo renderMenu("requests");
        ?>  

      <div id="page-wrapper">
	  	<div class="row">
          <div id='display-alerts' class="col-lg-12">

          </div>
        </div>
		<div class="col-lg-12">

			<!-- Button trigger modal -->
			<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
			  Add request
			</button>
			<hr>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
			      </div>
			      <div class="modal-body">
			        <form class="form-horizontal" role="form" name="updateAccount" action="update_user.php" method="post" padding-right: 0>
				      <div class="form-group" align='left'>
				      <label class="col-sm-4 control-label">New Item Name</label>
				      <div class="col-sm-4">
				        <input type="text" class="form-control" placeholder="" name='text1' value=' '>
				      </div>
				      </div>
				      <div class="form-group">
				      <label class="col-sm-4 control-label">Design Specifications</label>
				      <div class="col-sm-4">
				        <input type="text" class="form-control" placeholder="" name='text3'>
				      </div>
				      </div>
				      <div class="form-group">
				      <label class="col-sm-4 control-label">Printing Location</label>
				      <div class="col-sm-4">
				        <input type="text" class="form-control" placeholder="" name='text'>
				      </div>
				      </div>
				      <div class="form-group">
				      <label class="col-sm-4 control-label">Item Description</label>
				      <div class="col-sm-8">
				        <textarea class="form-control" rows="4"></textarea>

				      </div>
				      </div>
				      

				      <div class="form-group">
				      
				      </div>
				      <input type="hidden" name="csrf_token" value="52b84f05576d1c6cc6d57f2b7780d7d72e3d371556ec43565f04b2be3754822018b4c4a82efb5411a65ae369fb233cb87fa233e04ebd84eabb0e7b5679953926" />
				      <input type="hidden" name="user_id" value="0" />
				     </form>

			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary">Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-history"></i> Requests</h3>
              </div>




              <div class="panel-body">

              	<?php
				$result = mysql_query("SELECT * FROM `gp_requests`"); 
					while($row = mysql_fetch_array($result)){ 
						foreach($row AS $key => $value) {
							$row[$key] = stripslashes($value);	
							echo $row; 
						}
					// While - - Do this once for each... ie create row...
					//echo "<form name ='add_update' action='' method='POST'>";		
						echo "<tr>";  	
							echo "<td valign='top'>" . nl2br( $row['ID']) . "</td>";   
							echo "<td valign='top'>" . nl2br( $row['date']) . "</td>";
							echo "<td valign='top'><input type='text' name='image_name' value='" .stripslashes($row['notes']) ."'/></td>";
							echo "<td valign='top'><input type='text' name='image_position' value='" .stripslashes($row['tag']) ."'/></td>";
							echo "<td valign='top'><input type='text' name='image_file' value='" .stripslashes($row['location']) ."'/></td>";
							
						echo "</tr>";
					//echo "</form>";	
					}
				echo "</table>";
				
				/*
				if (isset($_POST['submbit_update'])) { 	
				
					$sql = "UPDATE `images` SET `image_name`= '{$_POST['image_name']}', `image_position`= '{$_POST['image_position']}', `image_file`= '{$_POST['image_file']}'
						WHERE `image_id` = '$????????'"; // $IMAGEid - cant grab this per UPDATE BUTTON
					mysql_query($sql) or die(mysql_error()); 
					echo (mysql_affected_rows()) ? "Edited row.   " : "Nothing changed.   "; 
					//echo "<a href='new.php'>Back To Listing</a>";
				}
				*/


				?>
				
                  
				<?php
				$con=mysqli_connect("localhost","root","cfg2014!","cfg");
				// Check connection
				if (mysqli_connect_errno()) {
				  echo "Failed to connect to MySQL: " . mysqli_connect_error();
				}

				$result = mysqli_query($con,"SELECT * FROM gp_requests");

				//$result = mysqli_query($con,"SELECT * FROM Persons");

				echo "
				<table table-bordered table-hover table-striped tablesorter >
				
				<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Notes</th>
				<th>Tag</th>
				<th>Location</th>
				<th>Image</th>
				<th>Attachments</th>
				</tr>

				";

				while($row = mysqli_fetch_array($result)) {
				  echo "<tr>";
				  //echo $row['ID'] . " " . $row['date'] . " " . $row['notes'] . " " . $row['tag'] . " " . $row['location'] . " " . $row['image'] . " " . $row['Attachments'];
				  echo "<td>" . $row['ID'] . "</td>";
  				  echo "<td>" . $row['date'] . "</td>";
  				  echo "<td>" . $row['notes'] . "</td>";
  				  echo "<td>" . $row['tag'] . "</td>";
  				  echo "<td>" . $row['location'] . "</td>";
  				  echo "<td>" . $row['image'] . "</td>";
  				  echo "<td>" . $row['attachments'] . "</td>";
				  echo "</tr>";
				}

				mysqli_close($con);
				?>
		        


				<!--
                <div id="transactions" class="table-responsive">
                  <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                      <tr>
                        <th>Request id <i class="fa fa-sort"></i></th>
                        <th>Request Date <i class="fa fa-sort"></i></th>
                        <th>Notes <i class="fa fa-sort"></i></th>
                        <th>Tag <i class="fa fa-sort"></i></th>
                        <th>Location <i class="fa fa-sort"></i></th>
                        <th>Images <i class="fa fa-sort"></i></th>
                        <th>Attachments <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>hammer, mechanics</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>hammer, mechanics</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>tools</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      
                    </tbody>
                  </table>
                </div>-->
                <div class="text-right">
                  <a href="#">View All Requests <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
	  </div>
	</div>


	
	<script>
        $(document).ready(function() {
          // Get id of the logged in user to determine how to render this page.
          var user = loadCurrentUser();
          var user_id = user['user_id'];
          
		  alertWidget('display-alerts');

		  // Set default form field values
		  $('form[name="updateAccount"] input[name="email"]').val(user['email']);

		  var request;
		  $("form[name='updateAccount']").submit(function(event){
			var url = APIPATH + 'update_user.php';
			// abort any pending request
			if (request) {
				request.abort();
			}
			var $form = $(this);
			var $inputs = $form.find("input");
			// post to the backend script in ajax mode
			var serializedData = $form.serialize() + '&ajaxMode=true';
			// Disable the inputs for the duration of the ajax request
			$inputs.prop("disabled", true);
		
			// fire off the request
			request = $.ajax({
				url: url,
				type: "post",
				data: serializedData
			})
			.done(function (result, textStatus, jqXHR){
				var resultJSON = processJSONResult(result);
				// Render alerts
				alertWidget('display-alerts');
				
				// Clear password input fields on success
				if (resultJSON['successes'] > 0) {
				  $form.find("input[name='password']").val("");
				  $form.find("input[name='passwordc']").val("");
				  $form.find("input[name='passwordcheck']").val("");
				}
			}).fail(function (jqXHR, textStatus, errorThrown){
				// log the error to the console
				console.error(
					"The following error occured: "+
					textStatus, errorThrown
				);
			}).always(function () {
				// reenable the inputs
				$inputs.prop("disabled", false);
			});
		
			// prevent default posting of form
			event.preventDefault();  
		  });

		});
	</script>
  </body>
</html>
