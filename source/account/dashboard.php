<?php
/*

UserFrosting Version: 0.2.1 (beta)
By Alex Weissman
Copyright (c) 2014

Based on the UserCake user management system, v2.0.2.
Copyright (c) 2009-2012

UserFrosting, like UserCake, is 100% free and open-source.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

require_once("../models/config.php");

if (!securePage(__FILE__)){
  // Forward to index page
  addAlert("danger", "Whoops, looks like you don't have permission to view that page.");
  header("Location: 404.php");
  exit();
}

setReferralPage(getAbsoluteDocumentPath(__FILE__));

?>

<!DOCTYPE html>
<html lang="en">
  <?php
  	echo renderAccountPageHeader(array("#SITE_ROOT#" => SITE_ROOT, "#SITE_TITLE#" => SITE_TITLE, "#PAGE_TITLE#" => "Dashboard"));
  ?>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
        <?php
          echo renderMenu("dashboard");
        ?>  

      <div id="page-wrapper">
	  	<div class="row">
          <div id='display-alerts' class="col-lg-12">
          
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <h1>Dashboard <small>User Overview</small></h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
            </ol>
            
          </div>
        </div><!-- /.row -->

        <!--
        <?php /*
        $result = mysql_query("SELECT * FROM `images` WHERE `imageMenu_id` = '$id' ORDER BY image_position ASC") or trigger_error(mysql_error()); 
          while($row = mysql_fetch_array($result)){ 
            foreach($row AS $key => $value) {
              $row[$key] = stripslashes($value);   
            }
          // While - - Do this once for each... ie create row...
          echo "<form name ='add_update' action='' method='POST'>";   
            echo "<tr>";    
              echo "<td valign='top'>" . nl2br( $row['image_id']) . "</td>";   
              echo "<td valign='top'>" . nl2br( $row['imageMenu_id']) . "</td>";
              echo "<td valign='top'><input type='text' name='image_name' value='" .stripslashes($row['image_name']) ."'/></td>";
              echo "<td valign='top'><input type='text' name='image_position' value='" .stripslashes($row['image_position']) ."'/></td>";
              echo "<td valign='top'><input type='text' name='image_file' value='" .stripslashes($row['image_file']) ."'/></td>";
              echo "<td valign='top'><input type='submit' value='Update Image' /><input type='hidden' value='1' name='submbit_update' />";
            echo "</tr>";
          echo "</form>"; 
          }
        echo "</table>";
        ?>-->

        <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-history"></i> Requests</h3>
              </div>
              <div class="panel-body">
                <div id="transactions" class="table-responsive">
                  <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                      <tr>
                        <th>Request id <i class="fa fa-sort"></i></th>
                        <th>Request Date <i class="fa fa-sort"></i></th>
                        <th>Notes <i class="fa fa-sort"></i></th>
                        <th>Tag <i class="fa fa-sort"></i></th>
                        <th>Location <i class="fa fa-sort"></i></th>
                        <th>Images <i class="fa fa-sort"></i></th>
                        <th>Attachments <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>hammer, mechanics</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>hammer, mechanics</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>10/11/2014</td>
                        <td>Design a hammer</td>
                        <td>tools</td>
                        <td>3.1333° N, 101.7000° E</td>
                        <td><a href="google.com">Image</a></td>
                        <td><a href="google.com">Attachments</a></td>
                      
                    </tbody>
                  </table>
                </div>
                <div class="text-right">
                  <a href="#">View All Requests <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

	<script>
        $(document).ready(function() {       
          alertWidget('display-alerts');
		});
	</script>
  </body>
</html>


