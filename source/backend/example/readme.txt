I. New Installation

1. Copy source code and Create Database
- Copy all the source code in the source directory to your web root directory.
- connect to mysql server, create a new database with the name is demo.

2. Configuration
- Open source/app/config/database.php and change your config mysql database server.

$config_database['default'] = array(
        'type' => 'mysql', // type: mysql|pdo
        'server' => 'localhost',            // server name.
        'user' => 'root',                   // user name connect to mysql server.
        'password' => '',                   // password of user.
        'database' => 'demo',               // database name.
);

2. Installation
- Access the App via browser and follow the instructions of the install page
- Login with users below:

    Administrator (full right) :
    account: admin 
    password: 123456

    Manager (read,edit,delete):
    account: manager 
    password: 123456

    User (read only):
    account: user 
    password: 123456 
    
    User (only access  their own records):
    account: user2
    password: 123456 

- finished, enjoy it.