<?php
/*

CODE FOR GOOD 2014
TEAM 2

*/

require_once("models/config.php");

// Public page

setReferralPage(getAbsoluteDocumentPath(__FILE__));

//Forward the user to their default page if he/she is already logged in
if(isUserLoggedIn()) {
	addAlert("warning", "You're already logged in!");
    header("Location: account");
	exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<style>
@font-face {
  font-family: 'Glyphicons Halflings';
  src: url('../fonts/glyphicons-halflings-regular.eot');
  src: url('../fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/glyphicons-halflings-regular.woff') format('woff'), url('../fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('../fonts/glyphicons-halflings-regular.svg#glyphicons-halflingsregular') format('svg');
}
</style>

  <?php
	echo renderTemplate("head.html", array("#SITE_ROOT#" => SITE_ROOT, "#SITE_TITLE#" => SITE_TITLE, "#PAGE_TITLE#" => "HOME Global Relief Print"));
  ?>

  <body>
    <div class="container">
      <div class="header">
        <ul class="nav nav-pills navbar pull-right">
        </ul>
        <h3 class="text-muted">Global Relief Print</h3>
      </div>
      <div class="jumbotron">
        <h1>Global Relief Print</h1>
        <p class="lead">Workflow technology + community <br> Enable easy 3D printing for good</p>
		    <h1><i class="fa fa-laptop"></i> <i class="fa fa-plus"></i> <i class="fa fa-users"></i> <b>=</b> <i class="fa fa-support"></i></h1>
    <div class="row">
			<div class="col-sm-12">
			  <a href="login.php" class="btn btn-success" role="button" value='Login'>Login</a>

			</div>
        </div>
        <div class="jumbotron-links">
        </div>
      </div>	
      <?php echo renderTemplate("footer.html"); ?>

    </div> <!-- /container -->

  </body>
</html>

<script>
	$(document).ready(function() {
		alertWidget('display-alerts');
        // Load navigation bar
        $(".navbar").load("header-loggedout.php", function() {
            $(".navbar .navitem-home").addClass('active');
        });
        // Load jumbotron links
        //$(".jumbotron-links").load("jumbotron_links.php");     
	});
</script>
