<?php
/*

CODE FOR GOOD 2014
TEAM 2

*/

require_once("models/config.php");

// Public page

setReferralPage(getAbsoluteDocumentPath(__FILE__));

//Forward the user to their default page if he/she is already logged in
if(isUserLoggedIn()) {
	addAlert("warning", "You're already logged in!");
    header("Location: account");
	exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<style>
@font-face {
  font-family: 'Glyphicons Halflings';
  src: url('../fonts/glyphicons-halflings-regular.eot');
  src: url('../fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/glyphicons-halflings-regular.woff') format('woff'), url('../fonts/glyphicons-halflings-regular.ttf') format('truetype'), url('../fonts/glyphicons-halflings-regular.svg#glyphicons-halflingsregular') format('svg');
}

html { height: 100% }
body { height: 100% }
#map-container { height: 30% }
</style>

  <?php
	echo renderTemplate("head.html", array("#SITE_ROOT#" => SITE_ROOT, "#SITE_TITLE#" => SITE_TITLE, "#PAGE_TITLE#" => "About Global Relief Print"));
  ?>

  <body>
    <div class="container">
      <div class="header">
        <ul class="nav nav-pills navbar pull-right">
        </ul>
        <h3 class="text-muted">Global Relief Print</h3>
      </div>
      
      <!-- About Section -->
    <section id="about" class="about-section">
   
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>About Section</h1>
                     <p>In times of need it is not always easy to get the necessary supplies, our goal is to crowd 
                source and get designs for 3D printed parts. When disaster strikes we need to get aid
                out as soon as possible, and the fastest way to do that is to make our own parts in times when it
                is hard to transport them from a different location.<p/>
                </div>
            </div>
        </div>
    </section>

    <!-- Gamification Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1><i class="fa fa-trophy"></i> Gamification</h1>
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Stats Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                      <li>Badges</li>
                      <li>Rewards</li>
                      <li>Collaboration</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <br>
    <!-- Story Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1><i class="fa fa-book"></i> Realtime story</h1>
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Story Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    See the journey of the request, design, review, and delivery. See activity in realtime on read only pages.
                    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>

                    
                </div>
            </div>
        </div>
    </section>

    <br>
     <!-- Social Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1><i class="fa fa-slideshare"></i> Social</h1>
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Social Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12ho">
                    Integrating latests social systems
                    <i class="btn-flickr"></i>
                    <br><br>
                    <button class="btn btn-facebook"><i class="fa fa-facebook"></i> | Connect with Facebook</button>
                    <button class="btn btn-twitter"><i class="fa fa-twitter"></i> | Connect with Twitter</button>
                </div>
            </div>
        </div>
    </section>

    <br>
    <!-- Tags Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1><i class="fa fa-bookmark"></i> Tags</h1>
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Tags Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Assing hashtasks to mission requests.
                    
                </div>
            </div>
        </div>
    </section>


    <!-- Contact Section 
    <section id="contact" class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Contact Section</h1>
                </div>
            </div>
        </div>
    </section>
    -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrolling-nav.js"></script>

     <script> 
 
      function init_map() {
    var var_location = new google.maps.LatLng(45.430817,12.331516);
 
        var var_mapoptions = {
          center: var_location,
          zoom: 14
        };
 
    var var_marker = new google.maps.Marker({
      position: var_location,
            map: var_map,
      title:"Venice"});
 
        var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);
 
    var_marker.setMap(var_map); 
 
      }
 
      google.maps.event.addDomListener(window, 'load', init_map);
 
    </script>
      <!-- /content end -->

      <?php echo renderTemplate("footer.html"); ?>

    </div> <!-- /container -->

  </body>
</html>

<script>
	$(document).ready(function() {
		alertWidget('display-alerts');
        // Load navigation bar
        $(".navbar").load("header-loggedout.php", function() {
            $(".navbar .navitem-about").addClass('active');
        });
        // Load jumbotron links
        //$(".jumbotron-links").load("jumbotron_links.php");     
	});
</script>
